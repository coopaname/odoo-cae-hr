# Copyright 2019 Coop IT Easy SCRL fs
#   Robin Keunen <robin@coopiteasy.be>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class EventRegistration(models.Model):
    _inherit = "event.registration"

    promotion_id = fields.Many2one(
        comodel_name="hr.promotion",
        string="Promotion",
        related="employee_id.promotion_id",
    )


class Event(models.Model):
    _inherit = "event.event"

    promotion_id = fields.Many2one(
        comodel_name="hr.promotion", string="Promotion", required=False
    )

    @api.multi
    def button_register_promotion(self):
        self.ensure_one()
        if not self.promotion_id:
            raise ValidationError(_("Enter a promotion first."))

        # get list of current registrations
        registrations = self.env["event.registration"].search(
            [("event_id", "=", self.id)]
        )
        # map list on employee, partner and email
        registrations_employee = registrations.mapped("employee_id")
        registrations_partner = registrations.mapped("partner_id")
        registrations_email = registrations.mapped("email")

        for employee in self.promotion_id.employee_ids:
            if (
                not employee in registrations_employee
                and not employee.work_email in registrations_email
            ):
                self.env["event.registration"].create(
                    {
                        "event_id": self.id,
                        "name": employee.name,
                        "email": employee.work_email,
                        "phone": employee.work_phone,
                        "employee_id": employee.id,
                    }
                )

        for applicant in self.promotion_id.applicant_ids:
            if (
                not applicant.emp_id
                and applicant.partner_id
                and not applicant.partner_id in registrations_partner
                and not applicant.partner_id.email in registrations_email
            ):
                self.env["event.registration"].create(
                    {
                        "event_id": self.id,
                        "name": applicant.partner_id.name,
                        "email": applicant.partner_id.email,
                        "phone": applicant.partner_id.phone,
                        "partner_id": applicant.partner_id.id,
                    }
                )

