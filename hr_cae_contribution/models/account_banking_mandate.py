from odoo import api, fields, models

import logging

_logger = logging.getLogger(__name__)


class AccountBankingMandate(models.Model):
    _inherit = "account.banking.mandate"

    send_date = fields.Date(
        string="Date of Request of the Mandate",
        required=True,
        default=fields.date.today(),
    )

    # ovrerrides super to add required = True
    partner_bank_id = fields.Many2one(
        comodel_name="res.partner.bank",
        string="Bank Account",
        track_visibility="onchange",
        domain=lambda self: self._get_default_partner_bank_id_domain(),
        ondelete="restrict",
        index=True,
        required=True,
    )

    # ovrerrides super to add reference to partner identification_id
    @api.model
    def create(self, vals=None):
        unique_mandate_reference = vals.get("unique_mandate_reference")
        if not unique_mandate_reference or unique_mandate_reference == "New":
            identification_id = (
                self.env["res.partner.bank"]
                .browse(vals.get("partner_bank_id"))
                .partner_id.identification_id
            )
            vals["unique_mandate_reference"] = (identification_id or "") + self.env[
                "ir.sequence"
            ].next_by_code("account.banking.mandate") or "New"
        return super(AccountBankingMandate, self).create(vals)
