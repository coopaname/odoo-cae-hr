# Copyright 2019 Coop IT Easy SCRL fs
#   Manuel Claeys Bouuaert <manuel@coopiteasy.be>
#   Robin Keunen <robin@coopiteasy.be>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class Employee(models.Model):
    _inherit = "hr.employee"

    contribution_exemption_ids = fields.One2many(
        comodel_name="hr.contribution.exemption",
        inverse_name="employee_id",
        string="Contribution Exemptions",
        required=False,
    )

    fixed_contribution_advance = fields.Selection(
        [
            ("PLV", "Prélèvement"),
            ("CHQ", "Chèque"),
            ("ESP", "Espèces"),
            ("PDA", "Pas d’avance"),
        ],
        string="Avance contribution fixe",
        default="PLV",
    )
