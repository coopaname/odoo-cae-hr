# Copyright 2019 Coop IT Easy SCRL fs
#   Robin Keunen <robin@coopiteasy.be>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models


class AttachmentCategory(models.Model):
    _name = "ir.attachment.category"
    _description = "Attachment Category"

    name = fields.Char()
    sequence = fields.Integer(default=0)

    def search(self, *argv, **kwargs):
        """
        Override search method to order on sequence, name
        """
        if "order" not in kwargs or not kwargs["order"]:
            kwargs["order"] = "sequence,name"

        return super().search(*argv, **kwargs)


class IrAttachment(models.Model):
    _inherit = "ir.attachment"

    expiration_date = fields.Date(string="Expiration Date", required=False)
    category_id = fields.Many2one(
        comodel_name="ir.attachment.category", string="Category", required=False
    )

    @api.onchange("category_id")
    def _onchange_category_id(self):
        if not self.name and self.category_id:
            self.name = self.category_id.name
